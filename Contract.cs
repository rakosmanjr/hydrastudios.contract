﻿// Copyright (C) HydraStudios
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
using System;
using System.Runtime.Serialization;
using System.Linq.Expressions;
using System.Collections;

namespace HydraStudios.Contract
{
	/// <summary>
	/// Helpers for checking code contract.
	/// </summary>
	public static class Contract
	{
		/// <summary>
		/// Throws a ContractException when certain conditions are not met.
		/// </summary>
		public static CEnsure Ensure => CEnsure.StaticInstance;

		public sealed class CEnsure
		{
			public static readonly CEnsure StaticInstance = new CEnsure();

			/// <summary>
			/// Parameter specific contracts.
			/// </summary>
			public CParam Param => CParam.StaticInstance;

			/// <summary>
			/// Throws if the check is not true.
			/// </summary>
			/// <example>
			/// Example for calling this method.
			/// <code>
			/// void Test(Int32 testValue)
			/// {
			///		Contract.Ensure.IsTrue(
			///			() => testValue != 0,
			///			"TestValue must not be zero",
			///			d => {
			///				d.Add("value", testValue)
			///			});
			/// }
			/// </code>
			/// </example>
			/// <param name="check">The condition</param>
			/// <param name="message">Message to include in the exception</param>
			/// <param name="dataProvider">Action to set the data on the generated exception</param>
			/// <exception cref="ContractException">Thrown when the condition is false</exception>
			public void IsTrue(Func<Boolean> check, String message, Action<IDictionary> dataProvider = null)
			{
				check = check ?? throw Helper.Make(Exceptions.Exception_check_cannot_be_null);

				Helper.ThrowIf(
					() => !check(),
					() => Helper.SetData(
						Helper.Make(message),
						dataProvider));
			}

			/// <summary>
			/// Throws if the check is true.
			/// </summary>
			/// <example>
			/// Example for calling this method.
			/// <code>
			/// void Test(Int32 testValue)
			/// {
			///		Contract.Ensure.IsFalse(
			///			() => testValue == 0,
			///			"TestValue must not be zero",
			///			d => {
			///				d.Add("value", testValue)
			///			});
			/// }
			/// </code>
			/// </example>
			/// <param name="check">The condition</param>
			/// <param name="message">Message to include in the exception</param>
			/// <param name="dataProvider">Action to set the data on the generated exception</param>
			/// <exception cref="ContractException">Thrown when the condition is true</exception>
			public void IsFalse(Func<Boolean> check, String message, Action<IDictionary> dataProvider = null)
			{
				check = check ?? throw Helper.Make(Exceptions.Exception_check_cannot_be_null);

				Helper.ThrowIf(
					check,
					() => Helper.SetData(
						Helper.Make(message),
						dataProvider));
			}
		}

		public sealed class CParam
		{
			public static readonly CParam StaticInstance = new CParam();

			/// <summary>
			/// Returns the name of the varible returned in the given lambda expression.
			/// </summary>
			/// <example>
			/// The expressions is expected to be in the form of:
			/// <code>
			/// () => {varible name}
			/// </code>
			/// </example>
			/// <typeparam name="T">Varible type</typeparam>
			/// <param name="param">Lambda expression</param>
			/// <returns>Varible name</returns>
			/// <exception cref="ContractException">Thrown if param is not in the correct form, or null</exception>
			public static String GetParamName<T>(Expression<Func<T>> param)
			{
				param = param ?? throw Helper.Make(Exceptions.Exception_param_expression_cannot_be_null);

				var member = param.Body as MemberExpression;
				var name = member?.Member.Name;

				return name ?? throw Helper.Make(Exceptions.Exception_param_expression_invalid);
			}

			/// <summary>
			/// Returns the value of the varible returned in the given lambda expression.
			/// </summary>
			/// <example>
			/// The expression is expected to be in the form of:
			/// <code>
			/// () => {varible name}
			/// </code>
			/// </example>
			/// <typeparam name="T">Varible type</typeparam>
			/// <param name="param">Lambda expression</param>
			/// <returns>Varible value</returns>
			/// <exception cref="ContractException">Thrown if param is not in the correct form, or null</exception>
			public static T GetParamValue<T>(Expression<Func<T>> param)
			{
				param = param ?? throw Helper.Make(Exceptions.Exception_param_expression_cannot_be_null);

				var compiled = param.Compile();

				return compiled();
			}

			/// <summary>
			/// Throws if the param is null.
			/// </summary>
			/// <example>
			/// Example for calling this method.
			/// <code>
			/// void Test(Object testObject)
			/// {
			/// 	Contract.Ensure.Param.IsNotNull(() => testObject);
			/// }
			/// </code>
			/// </example>
			/// <typeparam name="T">Param type</typeparam>
			/// <param name="param">A lambda expression returning the param</param>
			/// <param name="dataProvider">Action to set the data on the generated exception</param>
			/// <exception cref="ContractException">ArgumentNullException wrapped in a ContractException. Thrown when the param is null</exception>
			public void IsNotNull<T>(Expression<Func<T>> param, Action<IDictionary> dataProvider = null)
			{
				param = param ?? throw Helper.Make(Exceptions.Exception_param_expression_cannot_be_null);

				Helper.ThrowIf(
					() => GetParamValue(param) == null,
					() => Helper.Wrap(
						new ArgumentNullException(
							GetParamName(param),
							Exceptions.Exception_value_cannot_be_null)));
			}

			/// <summary>
			/// Throws if the param is not null.
			/// </summary>
			/// <example>
			/// Example for calling this method.
			/// <code>
			/// void Test(Object testObject)
			/// {
			/// 	Contract.Ensure.Param.IsNull(() => testObject);
			/// }
			/// </code>
			/// </example>
			/// <typeparam name="T">Param type</typeparam>
			/// <param name="param">A lambda expression returning the param</param>
			/// <param name="dataProvider">Action to set the data on the generated exception</param>
			/// <exception cref="ContractException">ArgumentNullException wrapped in a ContractException. Thrown when the param is not null</exception>
			public void IsNull<T>(Expression<Func<T>> param, Action<IDictionary> dataProvider = null)
			{
				param = param ?? throw Helper.Make(Exceptions.Exception_param_expression_cannot_be_null);

				Helper.ThrowIf(
					() => GetParamValue(param) != null,
					() => Helper.Wrap(
						new ArgumentNullException(
							GetParamName(param),
							Exceptions.Exception_value_must_be_null)));
			}

			/// <summary>
			/// Throws if the check function returns false.
			/// </summary>
			/// <example>
			/// Example usage:
			/// <code>
			/// void Foo(String test)
			/// {
			///		Contract.Ensure.Param.IsTrue(
			///			() => test,
			///			_ => !String.IsNullOrWhiteSpace(_),
			///			"String cannot be null or whitespace",
			///			d => d.Add(nameof(test), test));
			/// }
			/// </code>
			/// </example>
			/// <typeparam name="T">Param type</typeparam>
			/// <param name="param">Lambda expression returning the param</param>
			/// <param name="check">Predicate that takes the param as a value, returns false to throw an exception</param>
			/// <param name="message">User readable exception message</param>
			/// <param name="dataProvider">Optional action to store data in the generated exception</param>
			/// <exception cref="ContractException">ArgumentException wrapped in a ContractException. Thrown when check returns false</exception>
			public void IsTrue<T>(Expression<Func<T>> param, Predicate<T> check, String message, Action<IDictionary> dataProvider = null)
			{
				param = param ?? throw Helper.Make(Exceptions.Exception_param_expression_cannot_be_null);
				check = check ?? throw Helper.Make(Exceptions.Exception_param_check_cannot_be_null);

				Helper.ThrowIf(
					() => !check(GetParamValue(param)),
					() => Helper.Wrap(
						Helper.SetData(
							new ArgumentException(message, GetParamName(param)),
							dataProvider)));
			}

			/// <summary>
			/// Throws if the check function returns true.
			/// </summary>
			/// <example>
			/// Example usage:
			/// <code>
			/// void Foo(String test)
			/// {
			///		Contract.Ensure.Param.IsTrue(
			///			() => test,
			///			_ => !String.IsNullOrWhiteSpace(_),
			///			"String cannot be null or whitespace",
			///			d => d.Add(nameof(test), test));
			/// }
			/// </code>
			/// </example>
			/// <typeparam name="T">Param type</typeparam>
			/// <param name="param">Lambda expresion returning the param</param>
			/// <param name="check">Predicate that takes the param as a value, returns true to throw an exception</param>
			/// <param name="message">User readable exception message</param>
			/// <param name="dataProvider">Optional action to store data in the generated exception</param>
			/// <exception cref="ContractException">ArgumentException wrapped in a ContractException. Thrown when check returns false</exception>
			public void IsFalse<T>(Expression<Func<T>> param, Predicate<T> check, String message, Action<IDictionary> dataProvider = null)
			{
				param = param ?? throw Helper.Make(Exceptions.Exception_param_expression_cannot_be_null);
				check = check ?? throw Helper.Make(Exceptions.Exception_param_check_cannot_be_null);

				Helper.ThrowIf(
					() => check(GetParamValue(param)),
					() => Helper.Wrap(
						Helper.SetData(
							new ArgumentException(message, GetParamName(param)),
							dataProvider)));
			}
		}

		/// <summary>
		/// Helper functions for the main contract functions. Used interally and by contract extensions.
		/// </summary>
		public static class Helper
		{
			public static ContractException Make(String message) => new ContractException(message);
			public static ContractException Wrap(Exception exception) => new ContractException(exception.Message, exception);

			/// <summary>
			/// Throws the exception generated from factory if check is true.
			/// </summary>
			/// <param name="check">Returns true if an exception should be thrown</param>
			/// <param name="factory">Returns the type of exception to throw</param>
			/// <exception cref="System.Exception">Throws the exception created by the factory</exception>
			public static void ThrowIf(Func<Boolean> check, Func<Exception> factory)
			{
				Boolean c;

				try
				{
					c = check();
				}
				catch (Exception e)
				{
					throw Wrap(e);
				}

				if (c)
				{
					throw factory();
				}
			}

			/// <summary>
			/// Allows the given action to modify the contents of the gicen exception's data.
			/// </summary>
			/// <param name="exception">The exception whos data will be modified</param>
			/// <param name="dataProvider">The action responsible for modifying the exception's data</param>
			/// <returns>The passed in exception</returns>
			public static Exception SetData(Exception exception, Action<IDictionary> dataProvider)
			{
				dataProvider?.Invoke(exception.Data);

				return exception;
			}
		}
	}

	/// <summary>
	/// A contract exception is used to notify the caller when there is a bug in the way
	/// they are calling a funuction. The message is a description of what the contract
	/// violation was. Sometimes an inner exception is also set to give more detail.
	/// Additionally some violations will include extra information in the Data property.
	/// </summary>
	public class ContractException : Exception
	{
		public ContractException() { }
		public ContractException(String message) : base(message) { }
		public ContractException(String message, Exception innerException) : base(message, innerException) { }
		protected ContractException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
