﻿// Copyright (C) HydraStudios
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
using System;
using System.Linq;
using System.Linq.Expressions;
using HydraStudios.Contract;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace HydraStudios.Contract.Tests
{
	[TestFixture]
	public class ContractTests
	{
		[Test]
		public void Contract_Ensure_IsNotNull()
		{
			Assert.IsNotNull(Contract.Ensure);
		}

		[Test]
		public void ContractEnsure_Param_IsNotNull()
		{
			Assert.IsNotNull(Contract.Ensure.Param);
		}

		[Test]
		public void ContractEnsure_IsTrue_NullCheckThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.IsTrue(null, "");
			});
		}

		[Test]
		public void ContractEnsure_IsTrue_TrueCheckDoesNotThrow()
		{
			Assert.DoesNotThrow(() =>
			{
				Contract.Ensure.IsTrue(() => true, "");
			});
		}

		[Test]
		public void ContractEnsure_IsTrue_FalseCheckThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.IsTrue(() => false, "");
			});
		}

		[Test]
		public void ContractEnsure_IsFalse_NullCheckThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.IsFalse(null, "");
			});
		}

		[Test]
		public void ContractEnsure_IsFalse_FalseCheckDoesNotThrow()
		{
			Assert.DoesNotThrow(() =>
			{
				Contract.Ensure.IsFalse(() => false, "");
			});
		}

		[Test]
		public void ContractEnsure_IsFalse_TrueCheckThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.IsFalse(() => true, "");
			});
		}

		[Test]
		public void CParam_GetParamName_NullParamThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.CParam.GetParamName<Object>(null);
			});
		}

		[Test]
		public void CParam_GetParamName_InvalidExpressionThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.CParam.GetParamName(() => "".EndsWith("f"));
			});
		}

		[Test]
		public void CParam_GetParamName_ReturnsMemberName()
		{
			var testVariable = "";

			Assert.AreEqual(nameof(testVariable), Contract.CParam.GetParamName(() => testVariable));
		}

		[Test]
		public void CParam_GetParamValue_NullParamThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.CParam.GetParamValue<Object>(null);
			});
		}
		
		[Test]
		public void CParam_GetParamValue_ReturnsExpressionValue()
		{
			var testVariable = "asdf";

			Assert.AreEqual(testVariable, Contract.CParam.GetParamValue(() => testVariable));
		}

		[Test]
		public void ContractEnsureParam_IsNotNull_NullParamThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsNotNull<Object>(null);
			});
		}

		[Test]
		public void ContractEnsureParam_IsNotNull_NullObjectThrowsContractException()
		{
			Object test = null;

			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsNotNull(() => test);
			});
		}

		[Test]
		public void ContractEnsureParam_IsNotNull_NonNullObjectDoesNotThrow()
		{
			var test = new Object();

			Assert.DoesNotThrow(() =>
			{
				Contract.Ensure.Param.IsNotNull(() => test);
			});
		}

		[Test]
		public void ContractEnsureParam_IsNotNull_NullObjectThrowsArgumentNullExceptionInsideContractException()
		{
			Object test = null;

			try
			{
				Contract.Ensure.Param.IsNotNull(() => test);
			}
			catch (ContractException e)
			{
				Assert.IsInstanceOf<ArgumentNullException>(e.InnerException);
			}
		}

		[Test]
		public void ContractEnsureParam_IsNull_NullParamThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsNull<Object>(null);
			});
		}

		[Test]
		public void ContractEnsureParam_IsNull_NullObjectDoesNotThrow()
		{
			Object test = null;

			Assert.DoesNotThrow(() =>
			{
				Contract.Ensure.Param.IsNull(() => test);
			});
		}

		[Test]
		public void ContractEnsureParam_IsNull_NonNullObjectThrowsContractException()
		{
			var test = new Object();

			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsNull(() => test);
			});
		}

		[Test]
		public void ContractEnsureParam_IsNull_NonNullObjectThrowsArgumentNullExceptionInsideContractException()
		{
			var test = new Object();

			try
			{
				Contract.Ensure.Param.IsNull(() => test);
			}
			catch (ContractException e)
			{
				Assert.IsInstanceOf<ArgumentNullException>(e.InnerException);
			}
		}

		[Test]
		public void ContractEnsureParam_IsTrue_NullParamThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsTrue<Object>(null, _ => true, "");
			});
		}

		[Test]
		public void ContractEnsureParam_IsTrue_NullCheckThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsTrue<Object>(() => true, null, "");
			});
		}

		[Test]
		public void ContractEnsureParam_IsTrue_FalseCheckThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsTrue(() => new Object(), _ => false, "");
			});
		}

		[Test]
		public void ContractEnsureParam_IsTrue_TrueCheckDoesNotThrow()
		{
			Assert.DoesNotThrow(() =>
			{
				Contract.Ensure.Param.IsTrue(() => new Object(), _ => true, "");
			});
		}

		[Test]
		public void ContractEnsureParam_IsTrue_ThrowsArgumentExceptionWrappedInContractException()
		{
			var test = new Object();

			try
			{
				Contract.Ensure.Param.IsTrue(() => test, _ => false, "");
			}
			catch (ContractException e)
			{
				Assert.IsInstanceOf<ArgumentException>(e.InnerException);
			}
		}

		[Test]
		public void ContractEnsureParam_IsFalse_NullParamThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsFalse<Object>(null, _ => true, "");
			});
		}

		[Test]
		public void ContractEnsureParam_IsFalse_NullCheckThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsFalse<Object>(() => true, null, "");
			});
		}

		[Test]
		public void ContractEnsureParam_IsFalse_TrueCheckThrowsContractException()
		{
			Assert.Throws<ContractException>(() =>
			{
				Contract.Ensure.Param.IsFalse(() => new Object(), _ => true, "");
			});
		}

		[Test]
		public void ContractEnsureParam_IsFalse_FalseCheckDoesNotThrow()
		{
			Assert.DoesNotThrow(() =>
			{
				Contract.Ensure.Param.IsFalse(() => new Object(), _ => false, "");
			});
		}

		[Test]
		public void ContractEnsureParam_IsFalse_ThrowsArgumentExceptionWrappedInContractException()
		{
			var test = new Object();

			try
			{
				Contract.Ensure.Param.IsFalse(() => test, _ => true, "");
			}
			catch (ContractException e)
			{
				Assert.IsInstanceOf<ArgumentException>(e.InnerException);
			}
		}
	}
}
