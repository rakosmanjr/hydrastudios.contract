// Copyright (C) HydraStudios
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("HydraStudios.Contract.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HydraStudios")]
[assembly: AssemblyProduct("HydraStudios.Contract.Tests")]
[assembly: AssemblyCopyright("Copyright © HydraStudios 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("81e6f7fe-e5b8-419d-b0ac-9e2be1c686ff")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
